# Instructions

**Étape 0 :** Assurez-vous d'avoir configuré **Rasa** sur votre système ou serveur.

**Étape 1 :** Créez une application Slack en suivant les étapes ci-dessous :
  - Allez sur [Slack API: Applications](https://api.slack.com/apps) et cliquez sur "Create New App".
  - Donnez un nom à votre application et sélectionnez votre espace de travail Slack.
  - Sous "OAuth & Permissions", ajoutez les scopes suivants sous "Bot Token Scopes" : `app_mentions:read`, `channels:history`, `chat:write`, `im:history`, `im:write`, `users:read`.
  - Cliquez sur "Install App to Workspace" et autorisez l'application. Notez l'**OAuth Access Token**.
  - Copiez le **Signing Secret** depuis "Basic Information".

**Étape 2 :** Configurez les abonnements d'événements Slack :
  - Allez dans "Event Subscriptions" et activez les abonnements.
  - Utilisez `ngrok` pour exposer votre serveur local. Installez `ngrok` et exécutez :
    ```shell
    ngrok http 5005
    ```
  - Utilisez l'URL publique fournie par `ngrok` et ajoutez `/webhooks/slack/webhook`. Par exemple :
    ```
    https://abcd1234.ngrok.io/webhooks/slack/webhook
    ```
  - Ajoutez les événements `message.channels`, `message.im`, `app_mention`.

**Étape 3 :** Configurez le fichier **credentials.yml** avec les informations de votre application Slack par exemple :
```yaml
slack:
  slack_token: "xoxb-123456789012-123456789012-abcdef1234567890abcdef"
  slack_channel: "C12345678"
  slack_signing_secret: "abcdef123456abcdef123456abcdef12"

**Étape 4 :** Démarrez le serveur Rasa avec les commande suivante :
- rasa run -m models --enable-api --cors "*" --debug
- rasa run actions --cors "*" --debug
- Lancer la commande "ngrok 5005" sur ngrok

**Étape 5 :** Une fois que votre serveur Rasa est opérationnel, testez votre bot en envoyant des messages depuis votre canal Slack.

Bravo vous parlez maintenant à votre Bot Rasa :D
